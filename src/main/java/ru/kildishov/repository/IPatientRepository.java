package ru.kildishov.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ru.kildishov.model.Patient;


public interface IPatientRepository extends JpaRepository<Patient, Long>{

}
