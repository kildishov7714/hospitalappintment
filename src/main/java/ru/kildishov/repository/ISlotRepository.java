package ru.kildishov.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ru.kildishov.model.Slot;

public interface ISlotRepository extends JpaRepository<Slot, Long>{

}
