package ru.kildishov.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ru.kildishov.model.Doctor;


public interface IDoctorRepository extends JpaRepository<Doctor, Long>{

}
