package ru.kildishov.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;
import ru.kildishov.model.Patient;
import ru.kildishov.model.Slot;
import ru.kildishov.repository.IDoctorRepository;
import ru.kildishov.repository.IPatientRepository;
import ru.kildishov.repository.ISlotRepository;

@Slf4j
@Service
public class SlotService {
	private IDoctorRepository idr;
	private IPatientRepository ipr;
	private ISlotRepository isr;
	
	@Autowired
    public SlotService(IDoctorRepository idr, IPatientRepository ipr, ISlotRepository isr) {
        this.idr = idr;
        this.ipr = ipr;
        this.isr = isr;   
    }
	
	@Transactional
	public void save(Slot slot) {
		isr.save(slot);
	}
	 
	 @Transactional
	public void delete(Slot slot) {
		 isr.delete(slot);
	}
	 
	@Transactional
	public Slot getById(long id) {
		return isr.getById(id);
	}
	
	@Transactional
    public List<Slot> findAll(){
        return isr.findAll();

    }

}
