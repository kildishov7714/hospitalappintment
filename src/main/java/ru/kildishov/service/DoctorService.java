package ru.kildishov.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;
import ru.kildishov.exception.NotFoundException;
import ru.kildishov.model.Doctor;
import ru.kildishov.repository.IDoctorRepository;
import ru.kildishov.repository.IPatientRepository;
import ru.kildishov.repository.ISlotRepository;
@Slf4j
@Service
public class DoctorService {
	private IDoctorRepository idr;
	private IPatientRepository ipr;
	private ISlotRepository isr;
	
	@Autowired
    public DoctorService(IDoctorRepository idr, IPatientRepository ipr, ISlotRepository isr) {
        this.idr = idr;
        this.ipr = ipr;
        this.isr = isr;   
    }
	 @Transactional
	public void save(Doctor doctor) {
		idr.save(doctor);
	}
	 
	 @Transactional
	public void delete(Doctor doctor) {
		idr.delete(doctor);
	}
	 
	@Transactional
	public Doctor getById(long id) {
		return idr.getById(id);
	}
	
	@Transactional
    public List<Doctor> findAll(){
        return idr.findAll();

    }

		
	   


}
