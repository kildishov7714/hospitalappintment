package ru.kildishov.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;
import ru.kildishov.model.Doctor;
import ru.kildishov.model.Patient;
import ru.kildishov.repository.IDoctorRepository;
import ru.kildishov.repository.IPatientRepository;
import ru.kildishov.repository.ISlotRepository;
@Slf4j
@Service
public class PatientService {
	private IDoctorRepository idr;
	private IPatientRepository ipr;
	private ISlotRepository isr;
	
	@Autowired
    public PatientService(IDoctorRepository idr, IPatientRepository ipr, ISlotRepository isr) {
        this.idr = idr;
        this.ipr = ipr;
        this.isr = isr;   
    }
	
	@Transactional
	public void save(Patient patient) {
		ipr.save(patient);
	}
	 
	 @Transactional
	public void delete(Patient patient) {
		 ipr.delete(patient);
	}
	 
	@Transactional
	public Patient getById(long id) {
		return ipr.getById(id);
	}
	
	@Transactional
    public List<Patient> findAll(){
        return ipr.findAll();

    }


}
