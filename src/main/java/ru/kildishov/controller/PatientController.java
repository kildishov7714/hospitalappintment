package ru.kildishov.controller;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import ru.kildishov.model.Doctor;
import ru.kildishov.model.Patient;
import ru.kildishov.service.DoctorService;
import ru.kildishov.service.PatientService;

@RestController
@RequestMapping(path ="/patient", headers = "Accept=application/json")
@CrossOrigin(origins = "*", maxAge = 3600)
public class PatientController {
	
	private final PatientService ps;
	
	@Autowired
	public PatientController(PatientService ps) {
		this.ps = ps;
	}
	
	@PostMapping
    @ResponseStatus(HttpStatus.CREATED)
	public Patient save (@RequestBody Patient patient) {
		ps.save(patient);
		return patient; 
	}
	
	@GetMapping
    public List<Patient> findAll() {

       return ps.findAll();

    }

	 @GetMapping("/{id}")
	    
	    public Patient show(@PathVariable long id){
	        Patient patient = ps.getById(id);
	        return  patient;
	    }

}
