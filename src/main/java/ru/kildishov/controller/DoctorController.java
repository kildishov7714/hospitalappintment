package ru.kildishov.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import ru.kildishov.model.Doctor;
import ru.kildishov.service.DoctorService;

@RestController
@RequestMapping(path ="/doctor", headers = "Accept=application/json")
@CrossOrigin(origins = "*", maxAge = 3600)
public class DoctorController {
	
	private final DoctorService ds;
	
	@Autowired
	public DoctorController(DoctorService ds) {
		this.ds = ds;
	}
	
	@PostMapping
    @ResponseStatus(HttpStatus.CREATED)
	public Doctor save (@RequestBody Doctor doctor) {
		ds.save(doctor);
		return doctor; 
	}
	
	@GetMapping
    public List<Doctor> findAll() {

       return ds.findAll();

    }

}
