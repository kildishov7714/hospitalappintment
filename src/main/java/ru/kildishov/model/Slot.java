package ru.kildishov.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "slot")
public class Slot {
	 @Id
	 @GeneratedValue(strategy = GenerationType.AUTO)
	 @Column(name = "id", nullable = false)
	 private  long id;
	 @Column
	 private  Boolean free;
	 @Column
	 private Date date;
	 @Column
	 private long patientId;
	 @ManyToOne
	 private Doctor doctor;

}
